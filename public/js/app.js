$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
});
var fnName = "dosomething";
var pamName = '';
$(document).ready(function(){
    $('#btn_alert_ok').click(function(){
        $('#alertModel').modal('hide');
        window[fnName](pamName);
        });
});
function dosomething(){
};
function redirect(location){
    window.location.href = location;
}
function basicMsgAlert(msg, functionName, paramName)
{
    if(functionName != null) fnName = functionName;
    if(paramName != null) pamName = paramName;
    $('#msg_body').html(msg);
    // $('#alertModel').modal('toggle');
    $('#alertModel').modal({backdrop: 'static', keyboard: false})
};

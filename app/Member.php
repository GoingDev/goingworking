<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'user_ref', 'first_name', 'last_name' , 'gender', 'role', 'phone_number', 'email' , 'code' ,  'position', 'avatar_path'
    ];
}

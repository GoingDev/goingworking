<?php
//TeamManageController
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Member;
use App\Lov;

class TeamManageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $userList = $this->getUserList();
        return view('team.teamList', ['userList'=>$userList]);
    }

    private function getUserList()
    {
        return User::leftJoin('members', 'users.id', '=', 'members.user_ref')
        ->select('users.*', 'members.user_ref', 'members.first_name', 'members.last_name', 'members.position', 'members.code')->get();
    }

    public function NewMember($id)
    {
        $member = $this->getUserProfile($id);
        $user = User::where('id', '=', $id)->first();
        if(!$member) $member = new Member;
        if(!$user) $user = new User;
        $member->user_ref = $user->id;
        $member->first_name = $user->name;
        $member->email = $user->email;
        $genderList = Lov::where('lov_type', '=', 'gender')->get();
        $roleList = Lov::where('lov_type', '=', 'role')->get();
        return view('team.teamManage',
        [
            'member'=>$member,
            'genderList'=> $genderList,
            'roleList'=> $roleList
        ]);
    }

    private function getUserProfile($id)
    {
        return User::leftJoin('members', 'users.id', '=', 'members.user_ref')
        ->where('users.id', '=', $id)->first();
    }

    public function pageload(Request $request)
    {
        //return $request->input('id');
         return view('team.teamManage');
    }

    public function FindUser($id)
    {
        if(User::find($id)) return 'มี';
        return 'ไม่มี';
    }

    public function UpdateMember(Request $request)
    {
        return Member::create([
            'user_ref' => $request->user_ref,
            'first_name' => $request->fname,
            'last_name' => $request->lname,
            'gender' => $request->gender,
            'role' => $request->role,
            'phone_number' => $request->tel,
            'email' => $request->email,
            'code' => $request->code,
            'position' => $request->position,
            'avatar_path' => $request->avatar_path,
        ]);
    }
}

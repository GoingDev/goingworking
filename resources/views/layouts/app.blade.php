<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>GoingWorking - @yield('title')</title>
    <link rel="icon" href="{{ asset('img/logo-red.png') }}"/>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <!-- Fonts -->

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">
            <nav class="main-nav navbar navbar-expand-md" style="background-color: #00cec9;">
                    <div class="container">

                        <a class="main-logo navbar-brand" href="{{url('')}}">
                             <img src="{{ asset('img/logo-white.png')}}" alt="">
                        </a>

                        <!-- Toggler -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainMenu" aria-controls="mainMenu" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <i class="fal fa-bars"></i>
                        </button>
                        <!-- Toggler -->

                        <div class="collapse navbar-collapse" id="mainMenu">
                            <!-- Main Menu -->
                            <ul class="main-menu navbar-nav m-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('')}}">Dash Board
                                        <span class="sr-only">(current)</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('project')}}">Projects</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('client')}}">Clients</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('service')}}">Services</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('team')}}" class="nav-link">Team</a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{url('notice')}}" class="nav-link">Notice</a>
                                </li>
                            </ul>
                            <!-- Main Menu -->

                            <!-- Login Member -->
                            <div class="login-member d-flex align-items-center">
                                <div>
                                    <figure class="avatar">
                                        <img src="{{ asset('img/logo-red.png')}}" alt="">
                                    </figure>
                                </div>
                                <div class="dropdown">
                                    <p class="greeting small">Hello!, </p>
                                    <a class="name dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Administrator</a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="member.html">My Works</a>
                                        <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </nav>

            @yield('content')

        <footer class="main-footer text-center p-3 p-md-5">
                <div class="container">
                    <p class="copyright small">
                        ©COPYRIGHT. ALL RIGHT RESERVED
                    </p>
                </div>
         </footer>
    </div>
</body>
</html>

<!-- Modal -->
<div id="alertModel" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <div id="msg_body"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn_alert_ok">OK</button>
        </div>
      </div>
    </div>
</div>


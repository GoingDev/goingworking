@extends('layouts.app')
@section('title', 'NewPage')
@section('content')
    <!-- Page Header -->
    <div class="page-header">
            <div class="container">
                <h2 class="title">
                    Dash Board</h2>
                <div class="summary-card d-md-flex justify-content-between">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <h3 class="title">
                                <i class="fal fa-user"></i> Our Clients</h3>
                            <a href="client.html" class="small">View</a>
                        </div>

                        <div class="card-body">
                            <div class="row">

                                <div class="col-4">
                                    <p class="count">65</p>
                                    <p class="type">
                                        <i class="fal fa-user"></i>
                                        <span>Clients</span>
                                    </p>
                                </div>
                                <div class="col-8">
                                    <p class="desc">ขณะนี้มีลูกค้าทั้งหมด 98 ราย ลูกค้ารายล่าสุดคือ
                                        <a href="view-project-member.html" class="text-primary"> ทิพยประกันภัย</a>
                                    </p>
                                    <p class="thismonth">เดือนเมษายน 9 ราย</p>
                                </div>
                            </div>
                        </div>


                        <a href="add-client.html" class="btn-add">
                            <i class="fal fa-plus"></i>
                        </a>
                    </div>

                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <h3 class="title">
                                <i class="fal fa-folder"></i> Our Projects</h3>
                            <a href="project.html" class="small">View</a>
                        </div>

                        <div class="card-body">
                            <div class="row">

                                <div class="col-4">
                                    <p class="count">145</p>
                                    <p class="type">
                                        <i class="fal fa-folder"></i>
                                        <span>Projects</span>
                                    </p>
                                </div>
                                <div class="col-8">
                                    <p class="desc">ขณะนี้มีงานหมด 98 งาน งานล่าสุดคือ
                                        <a href="view-project-member.html" class="text-primary"> ทิพยประกันภัย</a>
                                    </p>
                                    <p class="thismonth">เดือนเมษายน 9 งาน</p>
                                </div>
                            </div>
                        </div>


                        <a href="add-project.html" class="btn-add">
                            <i class="fal fa-plus"></i>
                        </a>
                    </div>

                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <h3 class="title">
                                <i class="fal fa-cog"></i> Our Services</h3>
                            <a href="service.html" class="small">View</a>
                        </div>

                        <div class="card-body">
                            <div class="row">

                                <div class="col-4">
                                    <p class="count">99</p>
                                    <p class="type">
                                        <i class="fal fa-cog"></i>
                                        <span>Services</span>
                                    </p>
                                </div>
                                <div class="col-8">
                                    <p class="desc">ขณะนี้มีบริการทั้งหมด 67 บริการ ล่าสุดคือ
                                        <a href="view-project-member.html" class="text-primary"> ทิพยประกันภัย</a>
                                    </p>
                                    <p class="thismonth">เดือนเมษายน 9 บริการ</p>
                                </div>
                            </div>
                        </div>


                        <a href="add-service.html" class="btn-add">
                            <i class="fal fa-plus"></i>
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <!-- Page Header -->

        <!-- Notice -->

        <div class="gds-notice">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h3 class="title font-weight-bold text-primary">
                            <i class="fal fa-envelope-open"></i> Notice
                        </h3>
                        <p class="small">ทั้งหมด 9 ประกาศ</p>
                    </div>
                    <div class="col-sm-7">
                        <div class="notice-message">
                            <h4 class="font-weight-bold">Cum sociis natoque penatibus et magnis dis</h4>
                            <div class="message small text-secondary">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                                    massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                </p>
                            </div>
                        </div>
                        <nav>
                            <ul class="pagination pagination-sm">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">3</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>

                    </div>
                    <div class="col-sm-2 text-right">
                        <a href="notice.html" class="btn btn-primary">All Notices</a>
                    </div>
                </div>



            </div>
        </div>

        <!-- Section Title -->

        <section class="section-wrapper">
            <div class="container">
                <header class="section-head">
                    <div class="row">
                        <div class="col-sm-3">
                            <h2 class="title">
                                <i class="fal fa-folder"></i> Recent Projects
                            </h2>
                        </div>
                        <div class="col-sm-6">
                            <div class="search-bar">
                                <form>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fal fa-search"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="ค้นหา Projects" aria-label="Username" aria-describedby="basic-addon1">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <a href="add-project.html" class="btn btn-primary btn-block btn-md">
                                <i class="fal fa-plus"></i> Add New Project</a>
                        </div>
                    </div>

                </header>
                <article class="section-body">
                    <div class="project-list">

                        <table class="project-table table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th width="40%" class="title">Projects</th>
                                    <th>Clients</th>
                                    <th>Team</th>
                                    <th>Consultant</th>
                                    <th>Start Date</th>
                                    <th>Due Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="icon">
                                        <i class="fal fa-check"></i>
                                    </td>
                                    <td class="title">
                                        <a href="view-project-member.html">Professional Hi-Tech Supplies</a>
                                    </td>
                                    <td class="client">PTS</td>
                                    <td class="team">Treesoon</td>
                                    <td class="consultant">Chawanida</td>
                                    <td class="start">May 2, 2018</td>
                                    <td class="due">June 2, 2018</td>
                                </tr>

                                <tr>
                                    <td class="icon">
                                        <i class="fal fa-ellipsis-h-alt"></i>
                                    </td>
                                    <td class="title">
                                        <a href="view-project-member.html">Farm Design Thailand</a>
                                    </td>
                                    <td class="client">PTS</td>
                                    <td class="team">Treesoon</td>
                                    <td class="consultant">Chawanida</td>
                                    <td class="start">May 2, 2018</td>
                                    <td class="due">June 2, 2018</td>
                                </tr>

                                <tr>
                                    <td class="icon">
                                        <i class="fal fa-ellipsis-h-alt"></i>
                                    </td>
                                    <td class="title">SawatPaiboon</td>
                                    <td class="client">PTS</td>
                                    <td class="team">Treesoon</td>
                                    <td class="consultant">Chawanida</td>
                                    <td class="start">May 2, 2018</td>
                                    <td class="due">June 2, 2018</td>
                                </tr>

                                <tr>
                                    <td class="icon">
                                        <i class="fal fa-check"></i>
                                    </td>
                                    <td class="title">Professional Hi-Tech Supplies</td>
                                    <td class="client">PTS</td>
                                    <td class="team">Treesoon</td>
                                    <td class="consultant">Chawanida</td>
                                    <td class="start">May 2, 2018</td>
                                    <td class="due">June 2, 2018</td>
                                </tr>

                                <tr>
                                    <td class="icon">
                                        <i class="fal fa-ellipsis-h-alt"></i>
                                    </td>
                                    <td class="title">Farm Design Thailand</td>
                                    <td class="client">PTS</td>
                                    <td class="team">Treesoon</td>
                                    <td class="consultant">Chawanida</td>
                                    <td class="start">May 2, 2018</td>
                                    <td class="due">June 2, 2018</td>
                                </tr>

                                <tr>
                                    <td class="icon">
                                        <i class="fal fa-ellipsis-h-alt"></i>
                                    </td>
                                    <td class="title">SawatPaiboon</td>
                                    <td class="client">PTS</td>
                                    <td class="team">Treesoon</td>
                                    <td class="consultant">Chawanida</td>
                                    <td class="start">May 2, 2018</td>
                                    <td class="due">June 2, 2018</td>
                                </tr>

                                <tr>
                                    <td class="icon">
                                        <i class="fal fa-check"></i>
                                    </td>
                                    <td class="title">Professional Hi-Tech Supplies</td>
                                    <td class="client">PTS</td>
                                    <td class="team">Treesoon</td>
                                    <td class="consultant">Chawanida</td>
                                    <td class="start">May 2, 2018</td>
                                    <td class="due">June 2, 2018</td>
                                </tr>

                                <tr>
                                    <td class="icon">
                                        <i class="fal fa-ellipsis-h-alt"></i>
                                    </td>
                                    <td class="title">Farm Design Thailand</td>
                                    <td class="client">PTS</td>
                                    <td class="team">Treesoon</td>
                                    <td class="consultant">Chawanida</td>
                                    <td class="start">May 2, 2018</td>
                                    <td class="due">June 2, 2018</td>
                                </tr>

                                <tr>
                                    <td class="icon">
                                        <i class="fal fa-ellipsis-h-alt"></i>
                                    </td>
                                    <td class="title">SawatPaiboon</td>
                                    <td class="client">PTS</td>
                                    <td class="team">Treesoon</td>
                                    <td class="consultant">Chawanida</td>
                                    <td class="start">May 2, 2018</td>
                                    <td class="due">June 2, 2018</td>
                                </tr>

                            </tbody>
                        </table>

                    </div>

                </article>
            </div>
        </section>
@endsection

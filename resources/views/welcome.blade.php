<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h2>Register Form</h2>
        <div class="row col-lg-5">
            <h2>Get Request</h2>
            <button type="button" class="btn btn-warning" id="getRequest">GetRequest</button>
        </div>
        <div class="row col-lg-5">
            <h2>Register Form</h2>
            <form id="register">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <label for="firstname"></label>
                <input type="text" id="firstname" class="form-control">

                <label for="lastname"></label>
                <input type="text" id="lastname" class="form-control">

                <input type="submit" value="Register" class="btn btn-pramary">
            </form>
        </div>


        <div id='getRequestData'></div>
        <div id='postRequestData'></div>
    </div>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
        });
        $(document).ready(function(){
            $('#getRequest').click(function(){
                //alert($(this).text());
                $.get('getRequest', function(data){
                    $('#getRequestData').append(data);
                    console.log(data);
                });
            });

            $('#register').submit(function(){
                var fname = $('#firstname').val();
                var lname = $('#lastname').val();
                // $.post('postRequest', {firstname:fname, lastname:lname}, function(data) {
                //     $('#postRequestData').html(data);
                //     console.log(data);
                //     alert(data);
                // });
                var dataString = "firstname="+fname+"&lastname="+lname;
                $.ajax({
                    type : "POST",
                    url : "postRequest",
                    data : dataString,
                    success : function(data){
                        $('#postRequestData').html(data);
                        console.log(data);
                    }
                });

            });

        });
    </script>
</body>
</html>

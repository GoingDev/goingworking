@extends('../layouts.app')
@section('title', 'Team List')
@section('content')

<!-- Page Header -->
<div class="page-header">
    <div class="container">
        <div class="d-flex align-items-center justify-content-between">
            <h2 class="display-4 font-weight-bold"><i class="fal fa-users"></i> Team Members</h2>
            <div class="summary-title d-inline-flex">
                <div>
                    <p class="count display-4 title">12</p>
                    <p class="type">
                        <i class="fal fa-users"></i>
                        <span>Members</span>
                    </p>
                </div>
                <div class="ml-4">
                    <p class="desc small">ขณะนี้มีสมาชิกทั้งหมด 12 คน</p>
                    <p class="thismonth">สมาชิกใหม่คือ Surasit</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page Header -->

<!-- Section Title -->

<section class="section-wrapper">
    <div class="container">
        <header class="section-head">
            <div class="row">
                <div class="col-sm-3"><h2 class="title">
                <i class="fal fa-folder"></i> All Members
            </h2></div>
                <div class="col-sm-6"> <div class="search-bar">
                    <form>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">
                                    <i class="fal fa-search"></i>
                                </span>
                            </div>
                            <input type="text" id="txt_search" class="form-control" placeholder="ค้นหา Members" aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                    </form>
                </div></div>
                <div class="col-sm-3 text-right"><a href="#" id="btn_add_member" class="btn btn-block btn-primary btn-md">
                        <i class="fal fa-plus"></i> Add New Member</a></div>
            </div>

        </header>

        <article class="section-body">
            <div class="project-list">

                <table class="project-table table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Code</th>
                            <th width="40%" class="title">Members</th>
                            <th>Position</th>
                            <th>Join</th>
                            <th>Manage</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($userList as $item)
                        <tr>
                            <td class="icon">
                                <i class="fal fa-user"></i>
                            </td>
                            <td class="code">{{$item->code}}</td>
                            <td class="title"><a href="view-member.html">@if ($item->last_name)
                                {{$item->first_name}} {{$item->last_name}}
                            @else
                                {{$item->name}}
                            @endif</a></td>
                            <td class="position">{{$item->position}}</td>
                        <td class="join">{{$item->created_at}}</td>
                            <td class="manage small">@if ($item->user_ref)
                                <a href="#">Edit</a>
                            @else
                                <a href="/add-member/{{$item->id}}">New</a>
                            @endif | <a href="">Delete</a></td>
                        </tr>
                        @endforeach
                        <tr>
                            <td class="icon">
                                <i class="fal fa-user"></i>
                            </td>
                            <td class="code">GDS001</td>
                            <td class="title"><a href="view-member.html">Surasit Khamyan</a></td>
                            <td class="position">Web Developer</td>
                            <td class="join">2 May 1987</td>
                            <td class="manage small"><a href="#">Edit</a> | <a href="">Delete</a></td>

                        </tr>

                        <tr>
                                <td class="icon">
                                    <i class="fal fa-user"></i>
                                </td>
                                <td class="code">GDS002</td>
                                <td class="title"><a href="view-member.html">Surasak Malikee</a></td>
                                <td class="position">Project Manager</td>
                                <td class="join">2 May 1987</td>
                                <td class="manage small"><a href="#">Edit</a> | <a href="">Delete</a></td>
                            </tr>

                    </tbody>
                </table>

            </div>

        </article>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_add_member').click(function(){
            var id = $('#txt_search').val();
            window.location.href = 'add-member/'+id;
        });

    });
</script>
@endsection

@extends('../layouts.app')
@section('title', 'Add Member')
@section('content')

<!-- Page Header -->
<div class="page-header">
    <div class="container">
            <h2 class="font-weight-bold">
            <i class="fal fa-plus-circle"></i> Add Member</h2>
    </div>
</div>
<!-- Page Header -->

<!-- Section Title -->
<div class="container">
    <div class="add-box">

        <h3 class="mb-3">Member Descriptions</h3>
        <form id="memberForm" method="GET" action="/test">
        <input type="hidden" id="user_ref" name="user_ref" value="{{$member->user_ref}}">
            <div class="form-group row align-items-center">
                <label for="" class="col-sm-2">
                    Name </label>
                <div class="col-sm-4">

                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}{{$member->first_name}}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                </div>

                <label for="" class="col-sm-2">
                        Surname </label>
                    <div class="col-sm-4">

                        <input id="last_name" type="text" class="form-control" value="{{$member->last_name}}">
                    </div>

            </div>

            <div class="form-group row align-items-center">
                    <label for="" class="col-sm-2">
                        Gender
                    </label>
                    <div class="col-sm-4">
                            <select name="" id="gender" class="form-control">
                                @foreach ($genderList as $item)
                                <option value="{{$item->value}}" @if ($member->gender == $item->value)
                                        selected @endif>{{$item->display_value}}</option>
                                @endforeach
                            </select>
                    </div>

                    <label for="" class="col-sm-2">
                            Role
                        </label>
                        <div class="col-sm-4">
                            <select name="" id="role" class="form-control">
                                @foreach ($roleList as $item)
                                <option value="{{$item->value}}" @if ($member->role == $item->value)
                                        selected @endif>{{$item->display_value}}</option>
                                @endforeach
                            </select>
                        </div>
                </div>


            <div class="form-group row align-items-center">
                <label for="" class="col-sm-2">
                    Tel
                </label>
                <div class="col-sm-4">
                <input id="phone_number" type="text" class="form-control" value="{{$member->phone_number}}">
                </div>

                <label for="" class="col-sm-2">
                    Email
                </label>
                <div class="col-sm-4">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}{{$member->email}}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                </div>
            </div>

            <div class="form-group row align-items-center">
                    <label for="" class="col-sm-2">
                        Code </label>
                    <div class="col-sm-4">

                        <input id="code" type="text" class="form-control" value="{{$member->code}}">
                    </div>

                    <label for="" class="col-sm-2">
                            Position </label>
                        <div class="col-sm-4">

                            <input id="position" type="email" class="form-control" value="{{$member->position}}">
                        </div>

                </div>

                <div class="form-group row">
                        <div class="col-sm-2">
                            Add Avatar
                        </div>
                        <div class="col-sm-10">
                            <input id="avtar_path" type="file" class="form-control">
                        </div>
                    </div>


                    <div class="form-group row align-items-center">
                        <div class="col-sm-2">
                        </div>

                        <div class="col-sm-10">
                            <button type="button" class="btn btn-success" id="btn_save">Save</button>
                            <button type="button" class="btn btn-secondary" id="btn_cancel">Cancel</button>
                        </div>
                    </div>

        </form>

    </div>
</div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        if($('#user_ref').val() == '') basicMsgAlert('Account not found !', 'redirect', '/team');
        $('#btn_cancel').click(function(){
           window.location.href = '/team';
        });
        $('#btn_save').click(function(){
            //basicMsgAlert("พบข้อผิดพลาด!");
           addNewMember();
        //    $('#memberForm').submit();
        });

    });
    function addNewMember(){
        $.ajax({
            type : "GET",
            url : "/update-member",
            data : dataString(),
            success : function(data){
                console.log(data);
                // window.location.reload();
            }
        });
    };
    function dataString(){
        var user_ref = $('#user_ref').val();
        var fname = $('#name').val();
        var lname = $('#last_name').val();
        var gender = $('#gender').val();
        var role = $('#role').val();
        var tel = $('#phone_number').val();
        var email = $('#email').val();
        var code = $('#code').val();
        var position = $('#position').val();
        var file = $('#avatar_path').val();
        return {
            "user_ref" : user_ref,
            "fname" : fname,
            "lname" : lname,
            "gender" : gender,
            "role" : role,
            "tel" : tel,
            "email" : email,
            "code" : code,
            "position" : position,
            "file" : file,
        };
    };
</script>
@endsection

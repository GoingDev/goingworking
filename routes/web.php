<?php
//Route Bookmark
Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        return view('newPage');
    });
    Route::get('/project', function () {
        return view('home');
    });
    Route::get('/client', function () {
        return view('home');
    });
    Route::get('/service', function () {
        return view('home');
    });
    Route::get('/team', 'TeamManageController@index');
    Route::get('/notice', function () {
        return view('home');
    });
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/getRequest', 'AjaxController@index');

Route::post('/postRequest', 'AjaxController@index');

Route::get('/add-member/{id}', 'TeamManageController@NewMember');

Route::post('/add-member', 'TeamManageController@pageload');

Route::get('/update-member', 'TeamManageController@UpdateMember');